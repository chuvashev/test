const HEIGHT = 6;
const WIDTH = 5;
let CARDS = [];
let DECK = new ObservableArray([]);

window.onload = function() {
    createChooseCards();
    //createGrid();
    //for (let ib = 0; ib < CARDS; ib++) {
    //	createCard();
    //}
    DECK.addEventListener('itemadded', changeDeck);
    DECK.addEventListener('itemremoved', changeDeck);
};

function changeDeck() {
    let deckContainer = document.getElementById('deck');
    deckContainer.innerHTML = '<h5>Ваша дека</h5>';
    DECK.forEach(function(card, i){
        let cardContainer = document.createElement('div');
        cardContainer.className = 'deck-card';
        cardContainer.innerHTML += card.name.toUpperCase() + '<img src="static/close.svg" />';
        cardContainer.addEventListener('click', function () {
            DECK.splice(i, 1);
        });
        deckContainer.appendChild(cardContainer);
    });
}


function createChooseCards() {
    fetch('cards.json').then(function(response) {
        response.json().then(function(cards){
            let game = document.getElementById('game');
            let cardsContainer = document.createElement('div');
            cardsContainer.id = 'cards';
            let h3 = document.createElement('h3');
            h3.textContent = 'Первый этап. Выберите карты:';
            cardsContainer.appendChild(h3);
            CARDS = cards;
            cards.forEach(function(card, i){
                let cardContainer = document.createElement('div');
                cardContainer.className = 'card';
                cardContainer.innerHTML += '<h5>' + card.name.toUpperCase() + '</h5>';
                cardContainer.innerHTML += '<img src="static/price.svg" />' + card.value + '<br />';
                cardContainer.innerHTML += '<img src="static/attack.svg" />' + card.attack + '<br />';
                cardContainer.innerHTML += '<img src="static/health.svg" />' + + card.hp + '<br />';
                cardContainer.addEventListener('click', function () {
                    DECK.push(cards[i]);
                });
                cardsContainer.appendChild(cardContainer);
            });
            let deckContainer = document.createElement('div');
            deckContainer.id = 'deck';
            game.appendChild(deckContainer);
            game.appendChild(cardsContainer);
        });
    }).catch(err => console.error(err));
}

function createGrid() {
    let game = document.getElementById('game');
    let grid = document.createElement('div');
    grid.id = 'grid';
    let gridBody = document.createElement('div');
    grid.className = 'board';
    for (let jt = 0; jt < HEIGHT; jt++) {
        let row = document.createElement('div');
        row.className = 'row';
        for (let it = 0; it < WIDTH; it++) {
            let cell = document.createElement('div');
            cell.className = 'cell';
            cell.id = 'cell-' + it + '-' + jt;
            row.appendChild(cell);
        }
        gridBody.appendChild(row);
    }
    grid.appendChild(gridBody);
    game.appendChild(grid);
}

function createCard() {
    let cell = document.getElementById('cell-' + randomInt(0, HEIGHT) + '-' + randomInt(0, WIDTH));
    let card = document.createElement('div');
    let attack = document.createElement('div');
    let defense = document.createElement('div');
    card.className = 'card';
    attack.className = 'attack';
    attack.textContent = '7';
    defense.className = 'defense';
    defense.textContent = '12';
    card.addEventListener('click', function () {
        console.log('test');
    });
    card.appendChild(defense);
    card.appendChild(attack);
    cell.appendChild(card);
}

function randomInt(min, max) {
    return min + Math.floor((max - min) * Math.random());
}